## [1.0.1](https://gitlab.com/study-gp/terraform-modules/argo-cd-config/compare/v1.0.0...v1.0.1) (2021-08-25)

# [1.0.0](https://gitlab.com/study-gp/terraform-modules/argo-cd-config/compare/v0.1.0...v1.0.0) (2021-08-25)


### Bug Fixes

* **pbi00001:** edit repoURL in apps-system.yaml ([624915d](https://gitlab.com/study-gp/terraform-modules/argo-cd-config/commit/624915d6451e2cb4553b6f688a592dd54753f2c1))
* **pbi00003:** add main branch to CI config ([ff01c71](https://gitlab.com/study-gp/terraform-modules/argo-cd-config/commit/ff01c71f972a9b2433831686458b9d33b0900f3d))
* **pbi00003:** fix gitlab ci pipeline ([62fa38a](https://gitlab.com/study-gp/terraform-modules/argo-cd-config/commit/62fa38a102d6bf8fe82139160e0c31946b5463b9))


### Continuous Integration

* **pbi00003:** configure ci workflow for terraform module ([26d3129](https://gitlab.com/study-gp/terraform-modules/argo-cd-config/commit/26d31298112b47fd4c34f8e61e3c4c29acb80dfe))


### BREAKING CHANGES

* **pbi00003:** Now we have CI for our terraform module
