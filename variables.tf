variable "istio_namespace" {
  type    = string
  default = "istio-system"
}

variable "some_text" {
  type    = string
  default = "Hello, world!"
}