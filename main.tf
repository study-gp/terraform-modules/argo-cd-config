locals {
  apps_system      = "k8s-manifests/apps-system.yaml"
  resources_system = split("\n---\n", data.local_file.argocd_apps_system.content)
}

data "local_file" "argocd_apps_system" {
  filename = local.apps_system
}



# ----------------------------------------------------------------------------------------------------------------------
# ArgoCD Resources
# ----------------------------------------------------------------------------------------------------------------------

resource "k8s_manifest" "resources_system" {
  count = length(local.resources_system)

  timeouts {
    create = "5m"
    delete = "5m"
  }

  namespace = "kube-system"
  content   = local.resources_system[count.index]

}
